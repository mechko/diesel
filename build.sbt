name := "diesel"

version := "0.0.3"

scalaVersion := "2.11.7"

organization := "com.github.mrmechko"

//unmanagedBase := baseDirectory.value / "jar"
resolvers += Resolver.sonatypeRepo("snapshots")

libraryDependencies ++= Seq(
  "com.lihaoyi" %% "upickle" % "0.3.4",
  "com.github.mrmechko" %% "strips2" % "0.0.4f",
  "com.typesafe" % "config" % "1.3.0",
  "org.scala-lang.modules" %% "scala-xml" % "1.0.3",
  "org.scalatest" % "scalatest_2.11" % "2.2.4" % "test",
  "org.scala-lang.modules" %% "scala-parser-combinators" % "1.0.1",
  "com.assembla.scala-incubator" %% "graph-core" % "1.9.4",
  "com.github.omidb" %% "dgraph" % "0.1.0-SNAPSHOT"
)

//libraryDependencies += "cc.p2k" %% "centrality" % "1.0"


fork in run := true

parallelExecution in Test := false

javaOptions in run += "-Xmx8G"

javaOptions in run += "-Xms3G"

javaOptions in run +="-XX:+UseConcMarkSweepGC"
