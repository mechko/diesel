package diesel.TripsModules

import dgraph.DGraph
import diesel.SkeletonShapes.{SkeletonMatch, Skeleton, EdgeFiltrationUnit, PruneStrategy}

import scala.collection.immutable.TreeMap

import diesel.Core._
import FileOps._
import diesel.parser._
import diesel.parser.implicits._
import diesel.utils.SimpleStats._

object Score {

  def prune(s : Skeleton)(efu : EdgeFiltrationUnit = PruneStrategy.default) : Skeleton = {
    Skeleton(DGraph.from(s.l.nodes, s.l.edges.filter(e => efu(e._2.value))))
  }

  /**
  does gold have to be pruned?
  **/
  val gold : List[Skeleton] = readResource("/gold.data").getParseMap.values.flatMap(_.getDGraph).toList//.map(pruneG(_))

  def scoreList(t : List[Skeleton])(efu : EdgeFiltrationUnit = PruneStrategy.default) : Double = {
    val exp = t.map(_.l.outMap.count(_._2.nonEmpty)).sum
    t.flatMap(_.preds.collect{
      case Some(c) => c.scoreAgainst(gold)(efu)
    }).sum * 1.0/(exp)
  }

  def pred(tripsPred : String)(efu : EdgeFiltrationUnit = PruneStrategy.default) : Double = {
    //Prune the incoming skeleton and send it to be scored.  Gold will be pruned as found
    scoreList(tripsPred.getPred.map(s => prune(Skeleton(s))(efu)))(efu)
  }
}
