package diesel

import strips.ontology._
import strips.util.OntologyFromXML

object Core {
  implicit val ont = SOntology(OntologyFromXML(TripsConfig.tripsXMLPath+"/lexicon/data/"))

  case class FileOpContent(data : String) {
    def to(name : String) = {
      import java.io._
      val n = new PrintWriter(name)
      n.write(data)
      n.flush
      n.close
    }
  }

  object FileOps {
    def dumpText(data : String) : FileOpContent = FileOpContent(data)
    def readLines(filename : String) : String = {
      import scala.io.Source
      Source.fromFile(filename).getLines.mkString("\n")
    }
    def readResource(filename : String) : String = {
      import scala.io.Source
      Source.fromInputStream(getClass.getResourceAsStream(filename)).getLines.mkString("\n")
    }
  }


}

/**
 *  Miscellaneous tools to help with revising the ontology
 **/
object ReViz {
  private def maxDepth(node : String)(implicit ont : SOntology) : Int = {
    (ont --> node).map(_.children.map(c => maxDepth(c))).map(l => (l :+ 0).max + 1).getOrElse(0)
  }

  def layoutCVS(name : String, wpl : Boolean = true)(implicit ont : SOntology) : String = {
    val len = maxDepth(name)
    if(wpl) layoutCVSWPL(name, name, len)
    else layoutCVSW(name, name, len)
  }

  private def layoutCVSWPL(name : String, head : String, md : Int)(implicit ont : SOntology) : String = {
    val prefix = ((name +: ont.pathToRoot(name)).takeWhile(_!=head) :+ head).reverse
    ((prefix.padTo(md, "").mkString(",") +: (ont --> name).get.words.map(w => (prefix.padTo(md, "") :+ w).mkString(","))) ++ (ont --> name).get.children.map(c => layoutCVSWPL(c, head, md))).mkString("\n")
  }

  private def layoutCVSW(name : String, head : String, md : Int)(implicit ont : SOntology) : String = {
    val prefix = ((name +: ont.pathToRoot(name)).takeWhile(_!=head) :+ head).reverse
    (List((prefix.padTo(md, "") :+ (ont --> name).get.words.mkString(" ")).mkString(",")) ++ (ont --> name).get.children.map(c => layoutCVSW(c, head, md))).mkString("\n")
  }
}

object Parenthood {

  case class OntItemRel(i: SOntItem) {

    /**
     * TODO:70 This should probably return a string
     * @param ont: SOntology [description]
     * @return Returns the _Parent_ of `t` if they both exist (`t` and its parent)
     */
    def ^(implicit ont: SOntology) : Option[SOntItem] = {
      (ont --> i.parent)
    }

    /**
     *  Checks if i is a child of s in trips
     **/
    def ^?(s: String)(implicit ont: SOntology): Boolean = {
      ^#(s) > 0
    }

    def v(implicit ont: SOntology) : List[String] = {
      i.children
    }

    def ^?(o : SOntItem)(implicit ont : SOntology) : Boolean = ^?(o.name)

    /**
     *  Returns 0 if the types are the same, -1 if they aren't on the same pathToRoot
     *  and the distance if i is under s
     */
    def ^#(s : String)(implicit ont : SOntology): Int = {
      if (i.name == s) 0
      else{
        val res = ont.pathToRoot(i.name).indexOf(s)
        if (res > -1) res+1
        else res
      }
    }

    def ^#(o : SOntItem)(implicit ont : SOntology) : Int = ^#(o.name)

    def /@\(s : String)(implicit ont : SOntology): String = {
      val left = (i.name +: ont.pathToRoot(i.name)).reverse
      val right = (s +: ont.pathToRoot(s)).reverse

      left.zip(right).foldLeft(left(0)->right(0))((a,b) => if(b._1 == b._2) {b} else a)._1

    }

    def /@\(s : SOntItem)(implicit ont : SOntology): String = {
      val left = (i.name +: ont.pathToRoot(i.name)).reverse
      val right = (s.name +: ont.pathToRoot(s.name)).reverse

      left.zip(right).foldLeft(left(0)->right(0))((a,b) => if(b._1 == b._2) {b} else a)._1

    }

    def isTypeUnder(s : String)(implicit ont : SOntology) : Boolean = ^?(s)(ont)
  }

  case class WordRel(i : String) {
    /**
     * Returns the list of types that word i can take under type s
     **/
    def hasTypeUnder(s : String)(implicit ont : SOntology): List[SOntItem] = {
      ont.!@(i.toLowerCase).filter(o => (o ^? s))
    }
  }

  implicit def ontItem2rel(item : SOntItem) : OntItemRel = OntItemRel(item)

  implicit def string2OntItemRel(s : String)(implicit ont : SOntology) : OntItemRel = ontItem2rel(ont.-->(s).get)

  implicit def string2WordRel(s : String)(implicit ont : SOntology) : WordRel = WordRel(s.toLowerCase)

}
