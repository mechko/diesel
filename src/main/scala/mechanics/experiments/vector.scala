/**
 *
 *  The purpose of this experiment is to represent the skeletons as recursive
 *  vectors
 **/

package diesel.experiments

import dgraph.DGraph
import diesel.Parenthood._
import diesel.Core.ont
import diesel.SkeletonShapes.{PruneStrategy, EdgeFiltrationUnit, Skeleton}

object Walker {

  def step(d : DGraph[String, String]) : List[DGraph[String, String]] = {
    d.nodes.map(_._2.value).map(n => {
      d.mapByNodes(nn => {
        if (nn == n) (nn ^).map(_.name).getOrElse("root")
        else nn
      })
    })
  }.toList

  def stringifyd1pred(d : DGraph[String, String]) : String = {
    val em = d.edges.map(e => e._2.to -> e._2.value)
    d.nodes.map(n => ":"+ em.getOrElse(n._1, "_") + " " + n._2.value).toList.sorted.mkString("(", " ", ")")
  }

  trait BorderGenerator[T] {
    def interior : Traversable[T]
    def frontier : Traversable[T]
    def expandFrontier : Traversable[T] = collapse(frontier.flatMap(extend))

    def collapse(t : Traversable[T]) : Traversable[T] = {
      t.map(x => key(x) -> x).toMap.values.toList
    }

    def extend(e : T) : Traversable[T]

    /**
      * Override this for different equality measures
      **/
    def key(a : T) = a.toString

    def generate : BorderGenerator[T]
  }

  case class DGBG(
                   interior : List[DGraph[String, String]],
                   frontier : List[DGraph[String, String]]
                 ) extends BorderGenerator[DGraph[String, String]] {
    override def extend(a : DGraph[String, String]) : List[DGraph[String, String]] = step(a)
    override def key(d : DGraph[String, String]) = stringifyd1pred(d)

    override def generate: DGBG = {
      val newInt = collapse(interior ++ frontier)
      val nf = newInt.map(key).toSet
      val newFrt = expandFrontier

      DGBG(newInt.toList, newFrt.filterNot(a => nf.contains(key(a))).toList)
    }
  }

  object DGBG {
    private def rot(d : DGBG) : DGBG = {
      val e = d.generate
      if (e.frontier.size > 0) rot(e)
      else e
    }
    def apply(d : DGBG) : List[DGraph[String, String]] = {
      rot(d).interior
    }
    def core(d : DGraph[String, String], efu : EdgeFiltrationUnit = PruneStrategy.default) = {
      val g = List(Skeleton(d))

      val all = DGBG(DGBG(List(), List(d))).map(Skeleton(_))

      all.map(a => stringifyd1pred(a.l) -> a.preds.flatten.map(e => e.scoreAgainst(g)(efu)).mkString("[",",","]"))
    }.toMap
  }

}