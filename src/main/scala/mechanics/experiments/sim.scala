package diesel.experiments

import dgraph.DGraph
import diesel.SkeletonShapes.{PruneStrategy, EdgeFiltrationUnit}

import scala.util.Random

object sim {
  import diesel.Core._
  import FileOps._
  import diesel.parser._
  import diesel.parser.implicits._
  import diesel.utils.SimpleStats._

  import diesel.SkeletonShapes._

  case class Experiment(gold : Map[String, List[Skeleton]], parses : Map[String, List[Skeleton]], ind : Option[Int] = None, efu : EdgeFiltrationUnit = PruneStrategy.default) {
    val rand = new Random()

    val (test, corpus) = {
      ind match {
        case Some(i) => {
          val keys = gold.keys.toList.sorted
          List(keys(i)) -> keys.zipWithIndex.filterNot(_._2 == i).map(_._1)
        }
        case _ => {
          val a = rand.shuffle(gold.keys.toList).splitAt(1)
          a._1.toList -> a._2.toList
        }
      }
    }

    val GoldSkeletons = corpus.flatMap(c => gold(c))
    val testCases = test.map(t => t -> (
        scoreList(gold(t)) ,
        scoreList(parses(t)),
        gold(t).map(_.l.outMap.count(_._2.nonEmpty)).sum*1.0/gold(t).map(_.l.nodes.size).sum,
        parses(t).map(_.l.outMap.count(_._2.nonEmpty)).sum*1.0/parses(t).map(_.l.nodes.size).sum
      ))

      //Modify this to use the EFU version
    def scoreList(t : List[Skeleton]) : Double = {
      val exp = t.map(_.l.outMap.count(_._2.nonEmpty)).sum
      //Setting prune strategy here
      t.flatMap(_.preds.collect{case Some(c) => c.scoreAgainst(GoldSkeletons)(efu)}).sum * 1.0/(exp)
    }

    //testCases.foreach(println)

    val score = (
                  testCases.count(s => (s._2._1 > s._2._2)),
                  testCases.count(s => (s._2._1 != s._2._2))
                )

    val infinities = testCases.count(s => {
      (!s._2._2.isNegInfinity && !s._2._1.isNegInfinity)
    })//.sum

    val templ = """
                  |<html>
                  |  <head>
                  |    <script src="viz.js"></script>
                  |  </head>
                  |
                  |  <body>
                  |    %s
                  |  </body>
                  |</html>
                """.stripMargin

    import diesel.tools.SimExplorer._
    val tracer = Gold(corpus.map(gold).flatten)
    val trace = test.map(x => x -> gold(x)).map(s => templ.format("<h1>%s</h1>\n<h2>gold</h2>".format(s._1) + s._2.map(tracer.trace).mkString("\n") + "<h2>parser</h2>" + parses(s._1).map(tracer.trace).mkString("\n")))

    def printTraces(offset : Int = 0) = {
      trace.zipWithIndex.foreach(e => {
        import diesel.Core.FileOps._
          dumpText(e._1).to("skeletonTrace/%d.html".format(e._2+offset))
      })
    }

    def all = {
      import diesel.Core.FileOps._

      def m(dot : List[String]) = {
        "<div><script>var node = document.createElement(\"DIV\");" +
          "\n%s\ndocument.body.appendChild(node)</script>\n<p>------</p></div>".format(dot.map(a => {
          "node.appendChild(Viz(\"%s\", options = {format : \"svg\", engine : \"dot\"}));".format(a.replaceAll("\n", " "))
        }).mkString("\n"))
      }
      dumpText (templ.format(gold.values.map(_.map(x => diesel.tools.SimExplorer.dgraph2dot(x.l))).map(a => m(a)).map(a => "<div>%s</div>".format(a)).mkString("\n"))) to "all.html"
    }



  }
  def load(gold : String = "gold.data", parses : String = "parsed.data", ind : Option[Int] = None, e : EdgeFiltrationUnit = PruneStrategy.default) : Experiment = {
    //readLines(gold).getParseMap.keys.toList.sorted.zip(readLines(parses).getParseMap.keys.toList.sorted).foreach(println)
    Experiment(readLines(gold).getParseMap.mapValues(e => e.getDGraph), readLines(parses).getParseMap.mapValues(_.getDGraph), ind, efu = e)
  }

}

object determinism extends App {
  def det(efu : EdgeFiltrationUnit = PruneStrategy.default) = {
    val e = sim.load()
    val res = (1 to e.gold.size).map(x => {
      sim.load(ind = Some(x-1), e = efu)
    })


    val infinities = res.map(_.infinities).sum

    val ext = res.map(x => x.test.head -> x.score)
    val scores = ext.map(_._2)

    val (a, b) = scores.unzip

    println(a.sum + "/" + b.sum)
    println(b.sum + "/" + e.gold.size)
    val precision = (a.sum * 1.0 / b.sum)
    val recall = (b.sum * 1.0 / e.gold.size)
    println(precision)
    println(recall)
    println(2*recall*precision/(precision+recall))

    println(infinities)

    ext.toMap
  }

  trait reslt {
    def v : String
  }
  case object right extends reslt {override def v : String = "right"}
  case object wrong extends reslt {override def v : String = "wrong"}
  case object absent extends reslt {override def v : String = "absnt"}

  def interpret(t : (Int, Int)) : reslt = {
    if (t == (1,1)) right
    else if (t == (0,1)) wrong
    else absent
  }

  val all = det(PruneStrategy.nofilter)
  val red = det(PruneStrategy.reduced)
  val inv = det(PruneStrategy.inverse)

  all.keys.map(k => k -> List(all(k), red(k), inv(k)).map(s => interpret(s).v).mkString(" ")).toMap.foreach(println)

}

object test extends App {
  val s = (1 to 10).map(asdf => {
    val scores = (1 to 20).map(a => sim.load().score)

    scores.foreach(s => {
      printf("precision: %4f, recall: %4f\n", s._1 * 1.0 / (1 - s._2), (1 - s._2) * 1.0 / 1)
    })

    val (a, b) = scores.map(s => {
      (s._1, (1 - s._2))
    }).unzip

    (a.sum, b.sum)
  }).map(a => {
    printf("%d out of %d\n", a._1, a._2)
    a
  }).unzip

  println(s._1.sum *1.0/s._2.sum)

  //println(scores.flatMap(s => List(s._1, s._2)).size, scores.flatMap(s => List(s._1, s._2)).toSet.size)
}

object Tracer extends App {
  val (a,b) = (1 to 20).map(i => {
    val e = sim.load()
    val s = e.score
    printf("precision: %4f, recall: %4f\n", s._1*1.0/s._2, s._2*1.0/1)
    e.printTraces(i)
    s
  }).map(s => {
    (s._1, (1-s._2))
  }).unzip

  printf("%d out of %d\n".format(a.sum, b.sum))

  //sim.load().all
}

/*
object simDiscern {
  import diesel.Core._
  import FileOps._
  import diesel.parser._
  import diesel.parser.implicits._
  import diesel.skeleton._
  import diesel.utils.SimpleStats._

  case class SimDiscernExpInstance(test : Map[String, List[Twig]], gold : Map[String, List[Twig]], corpus : SkeletonList, sc : Int = 0) {
    lazy val run : (Int, Int) = {

      val testScores = test.map(e => {
        e._1 -> e._2.map(t => corpus.score(t)(sc)).sum/e._2.size
      })

      val goldScores = gold.map(e => {
        e._1 -> e._2.map(t => corpus.score(t)(sc)).sum/e._2.size
      })

      val res = testScores.keys.count(k => {
        printf("(%2f, %2f) ", testScores(k), goldScores(k))
        (testScores(k) > goldScores(k)) || (test(k).toSet.diff(gold(k).toSet).size == 0)
      }) -> test.size

      println()

      printf("testSizes: %f\ngoldSizes: %f\n\n", test.values.map(_.size*1.0).toList.mean, gold.values.map(_.size*1.0).toList.mean)
      res
      //(testScores.zip(goldScores).count(x => x._1 < x._2), test.size)
    }

    override def toString : String = {
      "%d / %d".format(run._1, run._2)
    }
  }

  case class SimDiscernExperiment(gold : Map[String, LF], parses : Map[String, LF]) {

    import scala.util.Random
    val r = new Random()

    def nFold(size : Int, sc : Int = 0) : List[SimDiscernExpInstance] = {
      val v = r.shuffle(gold.keys.toList).sliding(size, size).toList
      //s._1.foreach(println)

      v.map(k => {
        val s = (k.toList, gold.keys.filterNot(j => k.contains(j)).toList)
        val tst = s._1.map(x => x -> parses(x).getSkeletonList.flatten).toMap
        val gld = s._1.map(x => x -> gold(x).getSkeletonList.flatten).toMap

        val dg = s._2.map(x => parses(x).getDGraph).flatten.toList

        SimDiscernExpInstance(tst, gld, SkeletonList(dg), sc)
      })
    }

    def get(testSize : Int = 10, sc : Int = 0) : SimDiscernExpInstance = {
      val s = r.shuffle(gold.keys.toList).splitAt(testSize)

      //s._1.foreach(println)

      val tst = s._1.map(x => x -> parses(x).getSkeletonList.flatten).toMap
      val gld = s._1.map(x => x -> gold(x).getSkeletonList.flatten).toMap

      val dg = s._2.map(x => parses(x).getDGraph).flatten.toList

      SimDiscernExpInstance(tst, gld, SkeletonList(dg), sc)
    }
  }

  def makeParse(gold : String) = {
    import diesel.parser.local._
    readLines(gold).getParseMap.mapValues(_.repr.parse)
  }



  def battery(exp : SimDiscernExperiment, reps : Int = 20, size : Int = 10, scorer: Int = 0) : Unit = {
    if (reps > 0) {
      printf("rep %2d: %s\n", reps, exp.get(testSize = size, sc = scorer).toString)
      battery(exp, reps-1, size, scorer)
    }
  }

  def repeat(exp : SimDiscernExperiment, reps : Int = 20, size : Int = 10, scorer: Int = 0, score : (Int, Int) = (0,0)) : (Int, Int) = {
    if (reps > 0) {
      val res = exp.get(testSize = size, sc = scorer).run
      repeat(exp, reps-1, size, scorer, (score._1+res._1, score._2 + res._2))
    } else {
      score
    }
  }

  def standardBattery = {
    val l = load()
    val t = List("mean", "min", "max", "product")
    (0 to 3).foreach(s => {
      println(t(s))
      battery(l, scorer=s)
    })
  }

  def standardNFold = {
    val l = load()
    val t = List("mean", "min", "max", "product")
    val nf= l.nFold(10)

    nf.zipWithIndex.foreach(exp => {
      printf("Fold %2d:\n-------\n", exp._2)
      (0 to 3).foreach(s => {
        printf("%s: %s\n", t(s), exp._1.copy(sc = s))
      })
    })
  }
}
*/
