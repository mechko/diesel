package diesel.lisp

object Ontology {
    import diesel.sexpr._

    private val files = List("abstract-types", "physobj", "predicates",
                 "root-types", "situation-types", "social-contract",
                 "specific-situation-types", "speech-acts", "time-location-types")

    val basepath = "/Users/mechko/projects/co/DeepSemLex/trips/src/OntologyManager/Data/LFdata"

    def loadFile(f : String) = {
      import scala.io.Source

      Parser.parse(Source.fromFile("%s/%s.lisp".format(basepath, f))
        .mkString.split("\n")
        .map(l => l.split(";").headOption.getOrElse("")).mkString("\n").toLowerCase()).map(_.map(x => {
          x match {
            case y : SList => y.lst.grouped(2).toList
            case a : SAtom => List(List[SExpr](a))
          }
        }))
    }

    def test = {
      files.map(f => Ontology.loadFile(f).get.tail).map(_.map(x => {
        println(x.head)
        x.map(y => {
          println(y)
          ((y.head.asInstanceOf[SAtom].id) -> y.tail.head)
        })
      }))
    }

}
