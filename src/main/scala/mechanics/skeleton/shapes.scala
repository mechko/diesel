package diesel


import dgraph.{DGraph, NodeMatchAND, EdgeMatch, DEdge}
import strips.ontology.SOntItem

object SkeletonShapes {

  case class EdgeFiltrationUnit(f : String => Boolean) extends (String => Boolean) {
    def apply(edgelabel : String) : Boolean = f(edgelabel)
  }

  object PruneStrategy {
    val nofilter = EdgeFiltrationUnit((s : String) => true)

    def acceptedList(a : Set[String]) : EdgeFiltrationUnit = EdgeFiltrationUnit((s : String) => a.contains(s))
    def rejectedList(a : Set[String]) : EdgeFiltrationUnit = EdgeFiltrationUnit((s : String) => !a.contains(s))
    //ont::agent ont::agent1 ont::affected ont::affected1 ont::neutral ont::neutral1 ont::formal ont::result ont::affected-result ont::of ont::val ont::figure ont::ground
    val reduced  = acceptedList(Set("agent", "agent1", "affected", "affected1", "neutral", "neutral1", "formal", "result", "affected-result", "mod", "of", "val", "figure", "ground"))
    val inverse  = rejectedList(Set("agent", "agent1", "affected", "affected1", "neutral", "neutral1", "formal", "result", "affected-result", "mod", "of", "val", "figure", "ground"))
    val default  = reduced
  }




  import Core._
  import features._
  import distance._

  case class SkeletonList(l : List[Skeleton])

  implicit def dgraph2skeleton(l : DGraph[String, String]) : Skeleton = Skeleton(l)
  implicit def dgraphlst2skeletonlst(l : List[DGraph[String, String]]) : List[Skeleton] = l.map(Skeleton)


  import dgraph.DGraphDSL._
  import dgraph.{Node, EdgeMatchLike, NodeMatchLike, DGraph}
  abstract class GenericSkeleton(l : DGraph[String, String])

  abstract class PatternSkeleton(l : DGraph[String, String]) extends GenericSkeleton(l) {
    def childrenOf : SubgraphChildren = SubgraphChildren(l)
    def preds = {
      l.outMap.filter(o => o._2.size > 0).keys.toList.map(p => childrenOf(p))
    }
  }

  case class Skeleton(l : DGraph[String, String]) extends PatternSkeleton(l)

  type NMatch = NodeMatchLike[String]
  type EMatch = EdgeMatchLike[String]

  case class SkeletonMatch(l : DGraph[String, String]) {

    //dis baby need efu
    def queryShape(implicit efu : EdgeFiltrationUnit = PruneStrategy.default) = {
      import scala.collection.immutable.TreeMap

      //val used = l.edges.map(_._2.value).toList
      //println(used.mkString(", "), used.filter(efu(_)))

      val trimmed = l.edges.filterNot(ed => efu(ed._2.value)).map(_._1._2).toSet
      val newEdges = new TreeMap[(Int, Int), DEdge[EdgeMatchLike[String]]]() ++ l.edges.filter(ed => efu(ed._2.value)).mapValues(e => DEdge(
                      EdgeMatch[String]((a) => a == e.value).asInstanceOf[EdgeMatchLike[String]],
                      e.from,
                      e.to)
                    )
      val newNodes = l.nodes.filterNot(n => trimmed.contains(n._2.id)).map(
        n => n._1 -> Node(NodeMatchAND[String](a => true).asInstanceOf[NodeMatchLike[String]], n._2.id)
      )
      DGraph.from(
        newNodes, newEdges
      )
    }

    /**
      * We're assuming that you'll only ever pass a SkeletonShape of depth 1
      * @return a list of patterns each with an edge removed.
      */
    def deconstruct : List[SkeletonMatch] = {

      //val root = g.roots.head
      val edges = l.edges

      edges.map(e => {
        val newE = l.edges.filterNot(_ == e)
        val newN = l.nodes.filterNot(n => n._2.id == e._2.to)
        SkeletonMatch(DGraph.from(newN, newE))
      }).toList.filter(_.l.nodes.nonEmpty)
    }

    def dgroot(g : DGraph[String, String]) : Node[String] = {
      //println(g.nodes, g.edges)
      val (out, in) = g.edges.keys.unzip
      val ind = g.nodes.keySet.diff(in.toSet)
      g.nodes(ind.head)
    }

    val minScore : Double = Double. NegativeInfinity

//This is for backwards compatibility
    def scoreAgainst(skel : Skeleton) : Double = scoreAgainst(skel, false)(PruneStrategy.default)

    def scoreAgainst(skel : Skeleton, dec : Boolean)(implicit efu : EdgeFiltrationUnit) : Double = {
      val sr = (ont --> dgroot(l).value).get //the root of the SkeletonMatch
      //use the efu here
      val s = l.edges.values.map(a => a.value -> l.nodes(a.to).value).toMap.mapValues(v => (ont --> v).get)//.filter(ed => efu(ed._1))

      val qr = queryShape(efu)
      val candidates = skel.l.filter(qr)

      if (candidates.size == 0) {
        if (dec) (deconstruct.map(d => d.scoreAgainst(skel, true)) :+ minScore).max
        else minScore
        //minScore
      } else {

        //for each candidate
      (candidates.map(mtch => {
        //Take the root ont type
        val mr = (ont --> dgroot(mtch).value).get
        //and for each edge, take the edge type and the child node
        //use the efu here
        val m = mtch.edges.values.map(a => a.value -> mtch.nodes(a.to).value).toMap.mapValues(v => (ont --> v).get)//.filter(e => efu(e._1))

        val ns = math.log(nodeScore(sr, mr))
        ns + s.map(e => {
          math.log(m.get(e._1).map(v => nodeScore(e._2, v)) match {
            case Some(x) => {
              //println("not default")
              x
            }
            case _ => {
              //println("default")
              e._2.minMaxSim
            }
          })
        }).sum/s.size
      }) :+ minScore).max
    }
  }

    def scoreAgainst(skels : List[Skeleton])(implicit efu : EdgeFiltrationUnit = PruneStrategy.default) : Double = {
      skels.map(s => scoreAgainst(s, false)).max
    }
  }



  trait SkeletonShape {

    def l: DGraph[String, String]

    def apply(from: String): Option[SkeletonMatch] = {
      l.filterNodes(_ == from).headOption.map(n => apply(n))
    }

    def apply(from: Int): Option[SkeletonMatch] = {
      l.nodes.get(from).map(apply(_))
    }

    def apply(n: Node[String]): SkeletonMatch



  }

  case class SubgraphChildren(l: DGraph[String, String]) extends SkeletonShape {
    override def apply(n: Node[String]): SkeletonMatch = {
      val edges = l.edges.filter(_._1._1 == n.id)
      val nodes = (edges.map(_._2.to).toList :+ n.id).map(i => i -> l.nodes(i)).toMap

      SkeletonMatch(DGraph.from(nodes,edges))
      //SkeletonMatch(query[String, String](<&(_ == n.value, l.childsEdges(n.id).map(e => {
      //  -?>[String, String](_ == e.value, <&(_ == l.nodes(e.to).value))
      //}): _*)))
    }
  }


  // TODO:10 Twigs need node-ids like DGraph

  case class Twig(source : String, rel : String, target : String) {
    import dgraph.DGraphDSL._

    override def toString : String = "(%s %s %s)".format(rel, source, target)
    def qExact(graph : DGraph[String, String]) = {
      graph.filter(query[String, String](<&(t => (t == source), -?>(_ == rel, <&(t => (t == target))))))
    }

    def qListTarget(graph : DGraph[String, String]) = {
      graph filter query[String, String](<&(_ == source, -?>(_ == rel, <&(t => true))))
    }

    def qListSource(graph : DGraph[String, String]) = {
      graph filter query[String, String](<&(t => true, -?>(_ == rel, <&(_ == target))))
    }

    def qListBoth(graph : DGraph[String, String]) = {
      graph filter query[String, String](<&(t => true, -?>(_ == rel, <&(t => true))))
    }
  }

}
