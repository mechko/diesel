package diesel.utils


import diesel.Core._

object SimpleStats {
  case class Statistics(data : List[Double]) {
    lazy val mean = {
      if (data.size == 0) 0.0
      else data.sum/(1.0*data.size)
    }

    lazy val median = {
      if (data.size == 0) 0.0
      else data(data.size/2)
    }

    lazy val variance = {
      if (data.size == 0) 0.0
      else data.map(x => (x*1.0 - mean) * (x*1.0 - mean)).sum/(1.0*data.size)
    }

    lazy val stdev = {
      Math.sqrt(variance)
    }
  }

  import scala.language.implicitConversions
  implicit def ldouble2stat(x : List[Double]) = Statistics(x)
  implicit def lint2stat(x : List[Int]) = Statistics(x.map(_.toDouble))
  implicit def lfloat2stat(x : List[Float]) = Statistics(x.map(_.toDouble))
}

object WordNetLinked {
  // Provides the set of hypernyms around
  def outerBoundary(a : Iterable[String]) : Set[String] = {
    (a.flatMap(t => wn.bottle.simpleKey(t).asInstanceOf[wn.bottle.SimpleKey].hypernyms).toSet -- a.toSet )
  }

  def graphify(trips2boundary : Map[String, Set[String]]) : Map[String, Set[String]] = {
    val intermediate : Map[String, Set[String]] = trips2boundary.map(x => x._1 -> (TreeWeight.trips2wn.filter(y => y._2.toSet.intersect(x._2).size > 0).map(_._1).toSet))
    ont.ontItems.map(o => o.name -> ((intermediate.getOrElse(o.name, Set()) + (o.parent)))).toMap
  }

  def buildGraph : Map[String, Set[String]] = {
    graphify(TreeWeight.trips2wn.mapValues(outerBoundary))
  }

  def dump(graph : Map[String, Set[String]]) : String = {
    "nodes:\n" + {
      ont.ontItems.map(o => o.name + " " + TreeWeight.trips2wn.getOrElse(o.name, Set()).mkString(" ")).mkString("\n")
    } + "\n\nedges:\n" + {
      graph.map(x => x._1 + " " + x._2.mkString(" ")).mkString("\n")
    }
  }

  def dumpSubtreeWeights : String = {
    ont.ontItems.map(o => o.name + " " + TreeWeight.subtreeWeight(o.name)).mkString("\n")
  }

  def dumpSubtreeSize : String = {
    ont.ontItems.map(o => o.name + " " + TreeWeight.subtreeSize(o.name)).mkString("\n")
  }
}

object TreeWeight {
  import wn._
  def findHead(s : String) : String = bottle.simpleKey(s) match {
    case r : bottle.SimpleKey => r.key
    case _ => s
  }

  lazy val wordNetKeys = scala.io.Source.fromFile(diesel.TripsConfig.wordnetPath+"/dict/index.sense").mkString.split("\n").map(l => {
    val line = l.split(" ")
    line.head
  }).toList

  lazy val wnCounts = scala.io.Source.fromFile(diesel.TripsConfig.wordnetPath+"/dict/index.sense").mkString.split("\n").map(l => {
    val line = l.split(" ")
    (findHead(line.head), line.reverse.head.toInt+1) //perform +1 smoothing
  }).groupBy(_._1).mapValues(l => l.map(_._2).sum)

  import io.Source._
  lazy val synsetSizes = fromInputStream(getClass.getResourceAsStream("/heads")).mkString.split("\n").map(_.split(" ")).map(ss => ss.head -> ss.tail.size).toMap

  lazy val allwn = synsetSizes.keys.toList /*scala.io.Source.fromFile(diesel.TripsConfig.wordnetPath+"/dict/index.sense").mkString.split("\n").map(l => {
    l.split(" ")(0)
  }).toList.map(findHead).distinct*/

  lazy val pathLengths : List[Int] = allwn.flatMap(s => (ont !!# s).map(_._2.size))

  lazy val danglers = allwn.flatMap(k => (ont !!# k).map(e => (e._1, e._2.head, e._2.size))).groupBy(_._1).mapValues(l => l.groupBy(_._2).mapValues(ss => ss.map(_._3)))
  lazy val danglees = allwn.flatMap(k => (ont !!# k).map(e => (e._1, e._2.head, e._2.size))).groupBy(_._2).mapValues(l => l.groupBy(_._1).mapValues(ss => ss.map(_._3)))


  private lazy val keys = allwn.map(k => k -> (ont !# k).map(_.name))

  lazy val tripsWeights = keys.flatMap(e => e._2.map(t => t -> e._1)).groupBy(_._1).mapValues(s => s.map(w => wnCounts(w._2)).sum)

  lazy val trips2wn = keys.flatMap(e => e._2.map(t => t -> e._1)).groupBy(_._1).mapValues(s => s.map(w => w._2))

  def subtreeSize(s : String) : Int = {
      val t = (ont --> s).get
      val l = t.children.map(m => subtreeSize(m)).sum
      trips2wn.get(s).map(_.size).getOrElse(0) + l
  }

  def subtreeWeight(s : String) : Int = {
      val t = (ont --> s).get
      val l = t.children.map(m => subtreeWeight(m)).sum
      tripsWeights.get(s).getOrElse(0) + l
  }

  def profileSenseT(s : String) = (trips2wn.getOrElse(s, List()).size, subtreeSize(s), tripsWeights.getOrElse(s, 0), subtreeWeight(s))

  def profileSense(s : String) = {
    "| %s | %d | %d | %d | %d |".format(s, trips2wn.getOrElse(s, List()).size, subtreeSize(s), tripsWeights.getOrElse(s, 0), subtreeWeight(s))
  }

  def printTable(senses : List[String]) = {
    ("|sense|wn|wn_rec|weight|weight_rec|" +: ("|---------------------------------|" +: senses.map(profileSense))).mkString("\n")
  }

  //ttripsCount.toList.sortBy(_._2).foreach(println)
}
