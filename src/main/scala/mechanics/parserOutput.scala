package diesel

import dgraph._
import DGraphDSL._
import diesel.SkeletonShapes.Skeleton
import strips.ontology.SOntItem
import scala.collection.immutable.TreeMap


object parser {
  import SkeletonShapes.Twig

  /**
   * Convenience, if your config is properly created.
   */
  object local {
    implicit val purl = ParserUrl(TripsConfig.tripsParserLocal)
  }

  object remote {
    implicit val purl = ParserUrl(TripsConfig.tripsParserRemote)
  }

  /**
   * add an implicit parser location.  Limitation: Only one location can be
   * accessed at a time in a given scope
   * @param url  The base url to the parser
   * @param path The path from the base url if not /cgi/parse
   */
  case class ParserUrl(url : String, path : List[String] = List("cgi", "parse")) {
    def apply() : String = {
      Seq("http:/", url.stripPrefix("http://"), path.mkString("/")).mkString("/")
    }

    def get(text : String) : String = {
      import scala.xml._
      import java.net.{URL, URLEncoder}
      (XML.load(new URL(this()+"?input=%s".format(URLEncoder.encode(text,"UTF-8")))) \\ "terms" \\ "lisp")
        .map(_.text)
        .mkString("\n")
    }
  }

  /**
   * Create an LF directly.
   * @param repr : String [description]
   */
  case class LF(repr : String) {

    def getSkeletonList : List[Skeleton]= {
      parserUtils.DGraphFromText(repr).map(a => Skeleton(a))
    }

    def getDGraph : List[DGraph[String, String]] = {
      parserUtils.DGraphFromText(repr)
    }

    def getParseMap : Map[String, LF] = {
      parserUtils.uttCommentStyle(repr).mapValues(a => LF(a)).filterNot(_._1 == "")
    }

    def getPred : List[DGraph[String, String]] = {
      parserUtils.simplePredicate(repr)
    }
  }

  case class ParserRequest(repr : String)(implicit val url : ParserUrl) {
    def parse : LF = {
      LF(url.get(repr))
    }
  }

  object implicits {
    implicit def string2parserrequest(repr : String)(implicit purl : ParserUrl) : ParserRequest = ParserRequest(repr)
    implicit def string2LF(repr : String) : LF = LF(repr)
  }

}

// TODO:60 This is really the internals of the parser.  The parser object should just have the typeclasses and implicits.
// TODO:20 Deal with hashpipe comments.
object parserUtils {
  import diesel.sexpr.Parser
  import diesel.sexpr._
  import diesel.Core._
  import SkeletonShapes.Twig

  def simplePredicate(pred : String) : List[DGraph[String, String]]= {
    Parser.parse(cleanLines(pred)).get.map(_ match {
      case SList(inp) => {
        inp match {
          case SAtom("evaluate-skeleton") :: SList(args) :: Nil => {
            val r = args.collect{case SAtom(t) => t}.map(e => e.stripPrefix("ont::").stripPrefix(":"))
            val es = r.tail.grouped(2).map(a => --(a(0))->(Nd(a(1)))).toList
            Some(DGraph.from[String, String](Nd(r.head, es:_*)))
          }
          case _ => None
        }
      }
      case _ => None
    }).flatten
  }
  //TODO:30 Flatten all these methods and then lift them so we can build type-classes

  def cleanLines(input : List[String]) : String = {
    input.filterNot(_.startsWith(";")).map(_.split(";")(0)).mkString("\n").toLowerCase
  }
  def cleanLines(input : String) : String = cleanLines(input.split("\n").toList)

  trait sInfo

  case class sOr(values: List[String]) extends sInfo

  case class sText(word: String) extends sInfo

  case class sLabel(id: String) extends sInfo

  case class sIndex(num: Int) extends sInfo

  private def SExpr2sInfo(e: SExpr): sInfo = {
    e match {
      case SAtom(x) => {
        if (x.matches("\\d+")) {
          sIndex(x.toInt)
        } else if (x.startsWith("ont::")) {
          sLabel(x)
        } else {
          sText(x)
        }
      }
      case SList(l) => sOr(l.map(_.asInstanceOf[SAtom].id))
    }
  }

  case class sTerm(t: SOntItem, label: String, info: List[(String, sInfo)], isRoot: Boolean = false)

  type sExprList = Option[List[SExpr]]
  type sTermList = List[Option[sTerm]]

  def parse2sTermList(gp: sExprList) : List[sTermList] = {
    gp.get.collect {
      case SList(terms) => terms.collect {
        case SList(term) => {
          term match {
            /*case SAtom("ont::speechact") :: SAtom(label) :: SAtom(t) :: info => {
              //println(t)
              ont.get(t.stripPrefix("ont::")).map(v => {
                sTerm(v, label, info.grouped(2).map(p => p.head.asInstanceOf[SAtom].id -> SExpr2sInfo(p.tail.head)).toList, true)
              })
              None //HACK: Ignore speechacts altogether for the moment
            }*/
            //TODO:80 What if typeword is an atom instead of an slist
            case SAtom(t) :: SAtom(label) :: SList(typeword) :: info => {
              val tp = typeword.tail.collect{case SAtom(a) => a}.sorted.head //Pull the type out of type-word
              val preElements = info.grouped(2).map(p => p.head.asInstanceOf[SAtom].id -> SExpr2sInfo(p.tail.head)).toList
              val elements = preElements.find(_._2 == ":indicator") match {
                case Some(i) => preElements
                case None => preElements :+ (":indicator", sText(t.stripPrefix("ont::")))
              }
              ont.get(tp.stripPrefix("ont::")).map(v => sTerm(v, label, elements, false))
            }

            case SAtom(t) :: SAtom(label) :: SAtom(tp) :: info => {
              //println("NON-TYPEWORD:  " + tp)
              //This branch does the same as the branch above, except tp is guaranteed to be an ont type I HOPE
              val preElements = info.grouped(2).map(p => p.head.asInstanceOf[SAtom].id -> SExpr2sInfo(p.tail.head)).toList
              val elements = preElements.find(_._2 == ":indicator") match {
                case Some(i) => preElements
                case None => preElements :+ (":indicator", sText(t.stripPrefix("ont::")))
              }
              ont.get(tp.stripPrefix("ont::")).map(v => sTerm(v, label, elements, false))
            }
            case other => {
              //println(other)
              None
            }
          }
        }
      }
    }
  }

  def sTermList2DGraph(lst : List[sTermList]) : List[DGraph[String, String]] = {
    lst.map( e => {
      val g = e collect { case Some(st) if !st.t.name.startsWith("sa_") => {
        //println(st.t.name, st.label, st.info)
        st
      }
      }
      //println("--")

      val index = g.zipWithIndex

      val i2t = index.map(n => n._1.label -> n._2).toMap

      val nodes = index.map(n => n._2 -> Node(n._1.t.name, n._2)).toMap

      val edges = new TreeMap[(Int, Int), DEdge[String]]() ++ index.flatMap(s => {
        s._1.info.flatMap(l => {
          l match {
            case (rel, sLabel(t)) => i2t.get(t).map(i => (s._2, i) -> DEdge(rel.stripPrefix(":"), s._2, i))
            case _ => None
          }
        })
      })

      DGraph.from(nodes, edges)
    })
  }

  def sTermList2SkeletonList(lst: List[sTermList]) : List[List[Twig]]= lst.map( e => {

    val nodes = e collect {case Some(st) => (st.label) -> (st.t.name)} toMap
    val twi = e collect {case Some(st) => {(st.label) -> (st.t.name, st.info.filter(i => i._2.isInstanceOf[sLabel]))}}

    twi.flatMap(source => {
      source._2._2.map(rel => {
        //println(rel)
        (source._2._1, rel._1, nodes.get(rel._2.asInstanceOf[sLabel].id))
      }).collect { case (s : String, r : String, Some(t : String)) => Twig(s,r.stripPrefix(":"),t) }
      .toList
  })}.filterNot(t => {t.source.startsWith("sa_") || t.target.startsWith("sa_")})) //HACK: Remove speechacts

  def DGraphFromText(text : String) : List[DGraph[String, String]] = {
    sTermList2DGraph(parse2sTermList(Parser.parse(cleanLines(text))))
  }

  def uttCommentStyle(text : String) : Map[String, String] = {
    text.trim.split(";{4,}\n").toList
      .map(_.split("\n"))
      .map(x => x.head.stripPrefix(";;;").trim -> x.tail.mkString("\n"))
      .toMap
  }

  def SkeletonListFromText(text : String) : List[List[Twig]] = {
    sTermList2SkeletonList(parse2sTermList(Parser.parse(cleanLines(text))))
  }



}
