package diesel.angular

/**
 *   A bunch of serialization tools for creating forms with Textalk/angular-schema-form
 **/

 /*
 "array": {
      "title": "Array with enum defaults to 'checkboxes'",
      "type": "array",
      "items": {
        "type": "string",
        "enum": [
          "a",
          "b",
          "c"
        ]
      }
    }
*/

trait Question {
  def generate : String
}

case class CheckboxQuestion(key : String, title : String, options : List[String]) extends Question {
  override def generate : String = {
"""      "%s": {
        "type" : "object",
        "properties" : {
          "options" : {
            "title" : "%s",
            "type" : "array",
            "items" : {
              "type" : "string",
              "enum" : [%s]
            }
          },
          "other" : {
            "title" : "other",
            "type" : "string"
          }
        }
      }""".format(key, title, options.map(t => "\"%s\"".format(t)).mkString(", "))
  }
}

case class SimpleForm(title : String, questions : List[Question]) {
  def generate : String = {
    """
{
   "type" : "object",
  "title": "%s",
  "properties" : {
      "initials" : {
        "title": "initials",
        "type": "string"
      },
%s
  }
}
""".format(title, questions.map(_.generate).mkString(",\n        "))
  }

  def form : String = {
    """
    [
      "*",
      {
        "type" : "submit",
        "style": "btn-info",
        "title": "OK"
      }
    ]
    """
  }

  def js = {
    """
    var ref = new Firebase("https://ontonotes2trips.firebaseio.com/escapes");

    var onComplete = function(error) {
      if (error) {
         alert('Synchronization failed');
      } else {
         alert('Synchronization succeeded');
      } return false;
    };

    function form($scope) {
      $scope.schema = %s;

      $scope.form = %s;

      $scope.model = {};

      $scope.onSubmit = function(form) {

          ref.push($scope.model, onComplete)
      }
  }
    """.format(generate, form)
  }

  def html = {
    """
    <html ng-app="former">
    <head>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.0/angular.min.js"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.0/angular-sanitize.js"></script>
      <script type="text/javascript" src="https://raw.githubusercontent.com/geraintluff/tv4/master/tv4.min.js"/></script>
      <script type="text/javascript" src="https://raw.githubusercontent.com/mike-marcacci/objectpath/master/lib/ObjectPath.js"/></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/angular-schema-form/0.8.12/schema-form.min.js"/></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/angular-schema-form/0.8.12/bootstrap-decorator.min.js"/></script>
      <script src="https://cdn.firebase.com/js/client/2.3.1/firebase.js"></script>
      <script type="text/javascript">%s</script>
      <script type="text/javascript">
      var app = angular.module('former', ['schemaForm']);

      app.controller('FormController', form);
      </script>
    </head>
    <body>

      <div ng-controller="FormController">
          <form sf-schema="schema" sf-form="form" sf-model="model" ng-submit="onSubmit(myForm)"></form>
      </div>
    </body>
    """.format(js)
  }
}
