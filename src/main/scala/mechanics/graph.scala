package diesel.utils

import diesel.Core._

object Graphs {
  import scalax.collection.Graph // or scalax.collection.mutable.Graph
  import scalax.collection.GraphPredef._, scalax.collection.GraphEdge._
  import scalax.collection.edge.WLDiEdge     // labeled directed edge
  import scalax.collection.edge.Implicits._ // shortcuts
  import TreeWeight._
  import diesel.Parenthood._
  import wn.bottle

  trait GNode {
    def source : String
    def value : String
  }

  case class WNNode(value : String) extends GNode {
    override def source : String = "wn"
  }

  case class TripsNode(value : String) extends GNode {
    override def source : String = "trips"
  }

  case class OntoNode(value : String) extends GNode {
    override def source : String = "ontonotes"
  }

  object builder{
    def getWordNetEdges(k : String) = {
      bottle.simpleKey(k) match {
        case s : bottle.SimpleKey => s.hypernyms.map(h => WLDiEdge[GNode, String](WNNode(s.key) , WNNode(h))(1, "hypernym"))
        case _ => List()
      }
    }

    def wnSynsetEdges = {
      wordNetKeys.map(k => WLDiEdge[GNode, String](WNNode(k), WNNode(findHead(k)))(3, "synset"))
    }

    def wnHypernymGraph = Graph.from(allwn.map(WNNode), wnSynsetEdges ++ (allwn.flatMap(x => getWordNetEdges(x))))



    def getTripsEdges(k : String) = {
      (k ^) match {
        case Some(i) => List(WLDiEdge[GNode, String](TripsNode(k) , TripsNode(i.name))(2, "ancestor"))
        case None => List()
      }
    }

    def tripsHypernymGraph = Graph.from(ont.ontItems.map(t => TripsNode(t.name)), ont.ontItems.map(_.name).flatMap(n => getTripsEdges(n)))

    def wntripsedges = {
      val ontItems = ont.ontItems.map(_.name)
      val wte = {
        val mappings = allwn.flatMap(s => (ont !!# s)).groupBy(_._1).mapValues(_.map(value => value._2).flatten.distinct)
        //mappings.foreach(println)
        mappings.flatMap(pair => pair._2.map(s => WLDiEdge[GNode, String](WNNode(s) , TripsNode(pair._1))(4, "mapping")))
      }
      wte
    }

    def wnTripsGraph  = {
      val ontItems = ont.ontItems.map(_.name)
      val edges = allwn.flatMap(x => getWordNetEdges(x)) ++ ontItems.flatMap(getTripsEdges(_)) ++ wntripsedges ++ wnSynsetEdges
      val nodes : List[GNode] = ontItems.map(TripsNode(_)) ++ wordNetKeys.map(WNNode(_))
      Graph.from(nodes, edges)
    }
  }
}
