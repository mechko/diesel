package diesel.tools

import dgraph.DGraph
import diesel.SkeletonShapes.{SkeletonMatch, Skeleton}

import scala.collection.immutable.TreeMap

/**
  * Created by mechko on 12/7/15.
  */
object SimExplorer {

  def dgraph2dot(g : DGraph[String, String]) : String = {
    def escape(s : String) : String = {
      s.replaceAll("-", "_")
    }
    val nodes = g.nodes.values.map(n => {
      "%s [label=%s]".format(escape(n.value+n.id), escape(n.value))
    })

    val edges = g.edges.values.map( e => {
      val f = g.nodes(e.from)
      val to= g.nodes(e.to)
      "%s -> %s [label=%s]".format(escape(f.value+f.id), escape(to.value+to.id), e.value)
    })

    """
      |digraph S {
      |%s
      |
      |%s
      |}
    """.stripMargin.format(nodes.mkString("\t", ";\n\t", ";\n"), edges.mkString("\t", ";\n\t", ";\n"))
  }

  def graph(nodes : List[(String, Int)], edges : List[(Int, Int, String)]) : DGraph[String, String] = {
    import dgraph._

    val tm = new TreeMap[(Int, Int), DEdge[String]]() ++ edges.map(x => (x._1, x._2) -> DEdge[String](value=x._3, to=x._2, from=x._1))
    DGraph.from(nodes.map(x => x._2 -> Node[String](x._1, x._2)).toMap, tm)
  }

  def graph(nodes : String, edges : String) : DGraph[String, String] = {
    val n = nodes.trim.split("\n").zipWithIndex

    val e = edges.trim.split("\n").map(_.split(" ")).map(a => (a(0).toInt, a(1).toInt, a(2)))

    graph(n.toList,e.toList)
  }

  def graphList(file : String) : List[DGraph[String, String]] = {
    import diesel.Core.FileOps._

    readLines(file).split("\\+").toList.map(s => s.split("\\*\\*").toList).map(x => {
      graph(x(0), x(1))
    })
  }

  case class Gold(skeletons : List[Skeleton]) {
    def scoreSkeleton(other : Skeleton) : Double = {
      val exp = other.l.outMap.count(_._2.nonEmpty)
      other.preds.collect{case Some(c) => c.scoreAgainst(skeletons)}.sum * 1.0 / exp
    }

    def bestPred(other : SkeletonMatch) = {
      skeletons.map(s => s -> other.scoreAgainst(s)).maxBy(_._2)
    }

    def asHtml(other : Skeleton) : String = {
      """
        |<html>
        |  <head>
        |    <script src="viz.js"></script>
        |  </head>
        |
        |  <body>
        |    %s
        |  </body>
        |</html>
      """.stripMargin.format(trace(other))
    }

    def trace(other : Skeleton) : String = {

      def makeBody(r : List[(SkeletonMatch, (Skeleton, Double))]) : String = {
        def makeEntry(s : (SkeletonMatch, (Skeleton, Double))) : String = {
          ("<div><script>var node = document.createElement(\"DIV\");" +
            "var s = document.createTextNode(\"score = %f\");\nnode.appendChild(s);\n%s\n%s\ndocument.body.appendChild(node)</script>\n<p>------</p></div>").format(s._2._2,
            "node.appendChild(Viz(\"%s\", {format : \"png-image-element\"}));".format(dgraph2dot(s._1.l).replaceAll("\n", " ")),
            "node.appendChild(Viz(\"%s\", {format : \"png-image-element\"}));".format(dgraph2dot(s._2._1.l).replaceAll("\n", " ")))
        }
        r.map(makeEntry).mkString("\n")
      }

      val exp = other.l.outMap.count(_._2.nonEmpty)

      val results = other.preds.collect{case Some(c) => c -> bestPred(c)}


      val res = results.map(_._2._2).sum * 1.0/exp

      makeBody(results)
    }

  }
}
