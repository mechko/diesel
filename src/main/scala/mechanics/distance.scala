package diesel

import diesel.Core._
import diesel.features._
import strips.ontology.SOntItem

//import dgraph.{EdgeMatchLike, NodeMatchLike}

object distance {
  //TODO:40 Don't hardcode the maxDepthTrips. Instead compute it once for each version
  val maxDepthTrips = 22//ont.ontItems.map(t => ont.pathToRoot(t.name).size).max
  def pathDist(o1 : SOntItem, o2 : SOntItem) : Int = {
    if (o1 == o2) 0
    else {
      val paths = ont.pathToRoot(List(o1.name, o2.name)).map(_._2.reverse).toList

      //paths.flatten.size = over + a + over + b => distinct - over

      val over = paths(0).intersect(paths(1)).size
      val all = paths.flatten.distinct.size

      //printf("%s -> %s: %d\n", o1.name, o2.name, all-over+1)
      all - over + 1
    }
  }

  def pathDistP(o1: SOntItem, o2 : SOntItem) : Double = {
    (maxDepthTrips + 1 - pathDist(o1, o2)*1.0)/(maxDepthTrips+1)
  }

  def pathDistP(o1 : String, o2 : String) : Option[Double] = {
    ((ont --> o1) -> (ont --> o2)) match {
      case (Some(a), Some(b)) => Some(pathDistP(a,b))
      case _ => None
    }
  }

  implicit def ont2FeatureListDelta(ontItem : SOntItem) : FeatureListDelta = {
    val flt = {
      if (ontItem.sem.fltype.count(_ == ' ') > 0) {
        ontItem.sem.fltype.split(" ")(2)
      } else {
        ontItem.sem.fltype
      }
    }
    FeatureListDelta(featureList(flt), ontItem.sem.features)
  }

  def nodeScore(o1 : SOntItem, o2 : SOntItem) : Double = {
    pathDistP(o1, o2) * 1.0 * o1.maxSim(o2)
  }

  def nodeScore(o1 : String, o2 : String) : Double = {
    nodeScore((ont --> o1).get, (ont --> o2).get)
  }

  def featureScore(o1 : String, o2 : String) : Option[Double] = {
    (ont --> o1, ont --> o2) match {
      case (Some(a), Some(b)) => Some(a.maxSim(b))
      case _ => None
    }
  }

}
