package diesel.retired.parser

import java.net.{URL, URLEncoder}

import scala.xml._


object ParserUrl {
  case class ParserUrl(url : String, path : List[String] = List("cgi", "parse")) {
    def apply() : String = {
      Seq("http:/", url.stripPrefix("http://"), path.mkString("/")).mkString("/")
    }
  }
}

object parse {
  import ParserUrl._
  def parseWithGenre(inp : String, genre : String = "drum")(implicit p : ParserUrl) : Elem = {
    XML.load(new URL(p()+"?genre=%s&input=%s".format(genre, URLEncoder.encode(inp,"UTF-8"))))
  }

  def apply(sentence : String, genre : String = "drum")(implicit p : ParserUrl) : Parse = Parse(sentence, genre)

  case class TripsUtterance(words : Seq[String], tags : Seq[String], terms : List[sTerm])

  case class Parse(input : String, genre : String = "drum")(implicit parserUrl: ParserUrl) {
    lazy val xml = parseWithGenre(input, genre)
    lazy val res = (xml \\ "utt").map(utt => {
      //println((utt \\ "terms" \ "lisp").map(_.text))
      TripsUtterance(
        (utt \\ "words" \ "lisp").map(_.text),
        (utt \\ "tags" \ "lisp").map(_.text), //.map(w => TagList.parse(TagList.wordList, w.text).getOrElse(List[sWord]())),
        TermList.parse(TermList.termList, (utt \\ "terms" \ "lisp")
          .map(_.text).mkString("")
        ).getOrElse(List[sTermConstructor]()).map(_.toTerm))
    })
  }

  case class sTermConstructor(constructor : String,  varName : String, `type` : List[String], roles : List[(String, List[String])]) {
    def toTerm : sTerm = {
      sTerm(constructor, varName, `type`, roles.map(p=> {
        p match {
          case ("START", l : List[String]) => Boundary("START", Integer.parseInt(l.head))
          case ("END", l) => Boundary("END", Integer.parseInt(l.head))
          case ("WNSENSE", l) => Senses("WNSENSES", l)
          case (s, l) => Role(s, l.head)
        }
      }))
    }
  }

  case class sTerm(constructor : String, varName : String, `type` : List[String], roles : List[sTermTag])

  trait sTermTag {
    val key : String
  }

  case class Boundary(key : String, value : Int) extends sTermTag
  case class Role(key : String, value : String) extends sTermTag
  case class Senses(key : String, value : List[String]) extends sTermTag

  trait sTag

  case class TagHolder(key : String, value : sTag)
  case class SingleTag(value : String) extends sTag
  case class ListTag(values : List[String]) extends sTag
  case class NestedTag(values : List[TagHolder]) extends sTag

  case class sWord(kind: String, lemma: String, tags: List[TagHolder])


  //(<term constructor> <var-name> <type> [<role> <value>]*)
  import scala.util.parsing.combinator._

  object TermList extends JavaTokenParsers {
    def termList = "(" ~> term.* <~ ")"
    def term = "(" ~> tcons ~ varname ~ tpe ~ exprList <~ ")" ^^ {case c ~ v ~ t ~ r => sTermConstructor(c,v,t,r)}
    def tcons = token
    def varname = token
    def tpe = (token ^^ (x => List(x))) | "(" ~> ":*" ~> token.* <~ ")"
    def exprList : Parser[List[(String, List[String])]] = expr.*
    def expr: Parser[(String, List[String])] = (key~value) ^^ (x => (x._1, x._2))
    def key: Parser[String] = ":"~>token
    def value : Parser[List[String]] = token ^^ (x => List(x)) | "("~> token.* <~")"
    def token : Parser[String] = "[^\\s()]+".r
  }

  object TagList extends JavaTokenParsers {
    def wordList = "(" ~> word.* <~ ")"

    def word = "(" ~ token ~ token ~ keyVal.* <~ ")" ^^ { case t ~ w ~ k => List(t, w, k) }
    def wordName = token ^^ {case h => h.tail.reverse.tail.reverse}
    def keyVal : Parser[TagHolder] = key ~ value ^^ {case k ~ v => TagHolder(k, v)}
    def key = ":"~>token

    def value: Parser[sTag] = token ^^ (x => SingleTag(x)) | "(" ~> token.* <~ ")" ^^ (x => ListTag(x)) | "(" ~> keyValList <~ ")" ^^ (x => NestedTag(x))

    def keyValList: Parser[List[TagHolder]] = "(" ~> keyVal.* <~ ")"
    //def valueList = tokenList | "("~>  <~")"

    def token : Parser[String] = "[^\\s()]+".r
  }


  trait expr

  case class skey(v: String) extends expr

  case class svalue(v: String) extends expr

  case class elist(v: List[expr]) extends expr


  object ExprList extends JavaTokenParsers {
    def exprList: Parser[expr] = "(" ~> content.+ <~ ")" ^^ (x => elist(x))

    def content: Parser[expr] = key ~ valu ^^ { case k ~ v => elist(List(k, v)) } | exprList

    def key: Parser[expr] = token ^^ (x => skey(x))

    def valu: Parser[expr] = token ^^ (x => svalue(x)) | exprList

    //def elements : Parser[expr] = token.+ ^^ (x => elist(x)) | content
    def token: Parser[String] = "[^\\s()]+".r
  }

}

/**
 * WORD | PREFER -> "text"
 * DRUM -> (STRING) //ignore that piece
 * :FRAME -> (INT INT)
 * :SENSE-INFO -> (( info ))
 * info -> PENN-PARTS-OF-SPEECH | TRIPS-PARTS-OF-SPEECH
 * :PENN-PARTS-OF-SPEECH -> (STRING ...)
 * :TRIPS-PARTS-OF-SPEECH -> (STRING ...)
 * :UTTNUM -> INT
 * :DIRECTION -> STRING
 * :GENRE -> KEY
 * KEY -> ":"~>STRING
 * :PENN-CATS | :TRIPS-CATS -> ( STRING ... )
 */

object Test extends App {
  import parse._


  implicit val purl = ParserUrl.ParserUrl("127.0.0.1")
  println(purl())

  //println(Parse("\"this is an example\", said John.").res.map(_.tags.mkString(" ")).mkString("\n\n"))

  val t = "((WORD \"this\" :FRAME (1 5) :SENSE-INFO\n  ((:PENN-PARTS-OF-SPEECH (DT) :TRIPS-PARTS-OF-SPEECH (W::ART W::QUAN W::PRO)))\n  :UTTNUM 42 :DIRECTION INPUT :UTTNUM 42 :GENRE :DRUM)\n (PREFER \"this\" :FRAME (1 5) :PENN-CATS (NP) :TRIPS-CATS (W::NP) :UTTNUM 42\n  :DIRECTION INPUT :UTTNUM 42 :GENRE :DRUM)\n (WORD \"is\" :FRAME (6 8) :SENSE-INFO\n  ((:PENN-PARTS-OF-SPEECH (VBZ) :TRIPS-PARTS-OF-SPEECH (W::V))) :UTTNUM 42\n  :DIRECTION INPUT :UTTNUM 42 :GENRE :DRUM)\n (WORD \"an\" :FRAME (9 11) :SENSE-INFO\n  ((:PENN-PARTS-OF-SPEECH (DT) :TRIPS-PARTS-OF-SPEECH (W::ART W::QUAN W::PRO)))\n  :UTTNUM 42 :DIRECTION INPUT :UTTNUM 42 :GENRE :DRUM)\n (WORD \"example\" :FRAME (12 18) :SENSE-INFO\n  ((:PENN-PARTS-OF-SPEECH (NN) :TRIPS-PARTS-OF-SPEECH (W::N))) :UTTNUM 42\n  :DIRECTION INPUT :UTTNUM 42 :GENRE :DRUM)\n (WORD \"\\\"\" :FRAME (19 19) :UTTNUM 42 :DIRECTION INPUT :UTTNUM 42 :GENRE :DRUM)\n (PREFER \"an example\\\"\" :FRAME (9 19) :PENN-CATS (NP) :TRIPS-CATS (W::NP)\n  :UTTNUM 42 :DIRECTION INPUT :UTTNUM 42 :GENRE :DRUM)\n (PREFER \"is an example\\\"\" :FRAME (6 19) :PENN-CATS (VP) :TRIPS-CATS (W::VP)\n  :UTTNUM 42 :DIRECTION INPUT :UTTNUM 42 :GENRE :DRUM)\n (PREFER \"this is an example\\\"\" :FRAME (1 19) :PENN-CATS (S) :UTTNUM 42\n  :DIRECTION INPUT :UTTNUM 42 :GENRE :DRUM)\n (WORD \",\" :FRAME (20 21) :UTTNUM 42 :DIRECTION INPUT :UTTNUM 42 :GENRE :DRUM)\n (WORD \"said\" :FRAME (22 26) :SENSE-INFO\n  ((:PENN-PARTS-OF-SPEECH (VBD) :TRIPS-PARTS-OF-SPEECH (W::V))) :UTTNUM 42\n  :DIRECTION INPUT :UTTNUM 42 :GENRE :DRUM)\n (PREFER \"said\" :FRAME (22 26) :PENN-CATS (VP) :TRIPS-CATS (W::VP) :UTTNUM 42\n  :DIRECTION INPUT :UTTNUM 42 :GENRE :DRUM)\n (WORD \"John\" :FRAME (27 30) :SENSE-INFO\n  ((:PENN-PARTS-OF-SPEECH (NNP) :TRIPS-PARTS-OF-SPEECH (W::NAME)\n    :DOMAIN-SPECIFIC-INFO\n    ((DRUM\n      (SPECIALIST :EUI E0003544 :CITATION-FORM \"John\" :MATCHED-VARIANTS\n       (\"John\"))))))\n  :UTTNUM 42 :DIRECTION INPUT :UTTNUM 42 :GENRE :DRUM)\n (PREFER \"John\" :FRAME (27 30) :PENN-CATS (NP) :TRIPS-CATS (W::NP) :UTTNUM 42\n  :DIRECTION INPUT :UTTNUM 42 :GENRE :DRUM)\n (WORD \".\" :FRAME (31 31) :UTTNUM 42 :DIRECTION INPUT :UTTNUM 42 :GENRE :DRUM)\n (PREFER \"\\\"this is an example\\\", said John.\" :FRAME (0 31) :PENN-CATS\n  (SINV S SBARQ SQ) :UTTNUM 42 :DIRECTION INPUT :UTTNUM 42 :GENRE :DRUM))"
  println(ExprList.parse(ExprList.exprList, t))

  /** println(Parse("\"this is an example\", said John.").res.map(_.tags.map(w => {
    println("******parsing " + w)
    TagList.parse(TagList.wordList, w).getOrElse(List[sWord]())
  })).mkString("\n")) **/
}
