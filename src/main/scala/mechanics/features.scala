package diesel
import strips.ontology.SFeatureSet

/**
  * Created by mechko on 11/19/15.
  */
object features {
  import diesel.sexpr.Parser
  import diesel.sexpr._

  val alpha = 1.0
  case class FeatureListDelta(fl : FeatureList, featureSet : SFeatureSet) {
    val merged = featureSet.<^(fl.defaults)

    val additional = merged.feats.filterNot(t => fl.features.contains(t._1)).map(_._1)
    val all = (fl.features ++ additional).distinct
    val absent = all.filterNot(t => merged.feats.keySet.contains(t))
    val overridden = fl.defaults.feats.filter(x => merged.feats(x._1).sorted != x._2.sorted).map(m => merged.feats(m._1))

    def maxSim(other : FeatureListDelta) : Double = {

      ((maxOverlap(other) * 1.0) + alpha)/(size + epsilon + alpha)

    }

    def size : Int = this.all.size

    def epsilon : Double = math.max(0, features.avgFeatureSetSize-size)

    def maxDiff(other : FeatureListDelta) : Double = {
      minOverlap(other)*1.0/size
    }

    def minMaxSim = alpha/(size + epsilon + alpha)

    def maxOverlap(other : FeatureListDelta) : Int = {
      this.absent.intersect(other.all).size + merged.feats.toSet.intersect(other.merged.feats.toSet).size
    }

    def minOverlap(other : FeatureListDelta) : Int = {
      this.merged.feats.toSet.intersect(other.merged.feats.toSet).size
    }
  }

  case class FeatureList(name: String, features: List[String], defaults: SFeatureSet)

  case class FValue(name: String, parentOf: List[String], ancestorOf: List[String], childOf: Option[String])

  case class TValue(name: String, subValues: List[TValue]) {
    def flatten: List[FValue] = {
      val children = subValues.flatMap(_.flatten(name))
      FValue(name, subValues.map(_.name), children.map(_.name), None) +: children
    }

    def flatten(parent: String): List[FValue] = {
      val children = subValues.flatMap(_.flatten(name))
      FValue(name, subValues.map(_.name), children.map(_.name), Some(parent)) +: children
    }
  }

  private object TValue {
    def get(value: SExpr): Option[TValue] = {
      //println(value)
      value match {
        case SList(lst) => this.get(lst)
        case SAtom(v) => Some(TValue(v.stripPrefix("f::"), List()))
        case _ => None
      }
    }

    def get(value: List[SExpr]): Option[TValue] = {
      //println(value)
      value match {
        case SAtom(v) :: Nil => Some(TValue(v.stripPrefix("f::"), List()))
        case SAtom(v) :: rest => Some(TValue(v.stripPrefix("f::"), rest.map(r => {
          TValue.get(r)
        }).collect { case Some(vt: TValue) => vt }.toList))
        case _ => None
      }
    }
  }

  import diesel.Core.FileOps._
  val featureList = Parser.parse(readResource("/feature-types.lisp").split("\n").map(_.split(";")(0)).toList.mkString("\n").toLowerCase).get.tail.flatMap(l => {
    l match {
      case list: SList => Some({
        val fl = list.lst.grouped(2)

        FeatureList(fl.next().tail.head.asInstanceOf[SAtom].id.stripPrefix("f::"),
          fl.next().tail.head.asInstanceOf[SList].lst.map(_.asInstanceOf[SAtom].id.stripPrefix("f::")),
          SFeatureSet(fl.next().tail.head.asInstanceOf[SList].lst.map(default => {
            default.asInstanceOf[SList].lst.head.asInstanceOf[SAtom].id.stripPrefix("f::") -> (default.asInstanceOf[SList].lst.tail.head match {
              case SAtom(v) => List(v.stripPrefix("f::"))
              case SList(vs) => vs.tail.tail.map(v => v.asInstanceOf[SAtom].id.stripPrefix("f::"))
            })
          }).toMap))
      })
      case atom: SAtom => None;
    }
  }).map(fl => {
    fl.name -> fl
  }).toMap + ("nil" -> FeatureList("nil", List(), SFeatureSet(Map())))

  val avgFeatureSetSize = featureList.values.map(_.features.size).sum*1.0/featureList.size

  //implicit def slist2list(s : SList) : List[String] = s.lst
  //implicit def satom2string(s:SAtom) : String = s.id



  //  println(fdecs)

  private val featureDecls = Parser.parse(readResource("/feature-declarations.lisp")
    .split("\n").toList.filterNot(_.startsWith(";")).map(_.split(";")(0)).mkString("\n").toLowerCase).map { y =>
    y.tail.flatMap(l => {
      l match {
        case list: SList => {
          val fl = list.lst.grouped(2).toList

          val decl = fl.head.map(_.asInstanceOf[SAtom].id.toLowerCase.stripPrefix("f::"))

          decl match {
            case Nil => None
            case "define-feature" :: f => {
              //println("------------")
              //println(fl.tail.head)
              //println("------------")
              val fs = fl.tail.head match {
                case SAtom(":values") :: x => {
                  //println("> "+ x)
                  Some(x.head.asInstanceOf[SList].lst.map { y => TValue.get(y) })
                }
                case _ => None
              }
              fs.map(e => f.head -> e.flatten) match {
                case Some(r) => {
                  //println(r)
                  Some(Right(r))
                }
                case _ => None
              }
              //Right(f)
            }
            case "define-feature-rule" :: r => {
              Some(Left(r))
            }
            //case "define-feature-arguments" :: x => None
            case _ => None
          }

        }
      }
    })
  }.get

  val featureDefinitions = featureDecls.collect{case Right(r) => r}.toMap




}
