package diesel.SkeletonScore


import dgraph.{Node, DGraph, DEdge}
import diesel.SkeletonScore

import scala.collection.immutable.TreeMap

trait EdgeAccepter {
  def accept(s : String) : Boolean
}

trait AcceptAllEdges extends EdgeAccepter {
  def accept(s : String) : Boolean = true
}

trait ReducedEdges extends AcceptCommonEdges {
  override val common = Set("agent", "affected", "neutral", "formal", "result")
}

trait AcceptCommonEdges extends EdgeAccepter {
  val common = Set("agent", "agent1", "affected", "affected1", "neutral", "neutral1", "formal", "result", "affected-result", "figure", "ground")
  override def accept(s : String) : Boolean = common.contains(s)
}

trait RejectCommonEdges extends AcceptCommonEdges {
  override def accept(s : String) : Boolean = !common.contains(s)
}

/**
  * Grants the ability to score two trips types
  */
trait NodeScorer {
  def node(o1 : String, o2 : String) : Double
}


/**
  * Grants the ability to score two edges (labels only)
  */

trait EdgeScorer {
  def edge(e1 : String, e2 : String) : Double
}


case class SLink(_1 : String, _2 : String,_3 : String) {
  def lisp : String = "(:%s %s %s)".format(_2, _1, _3)
}
object SLink {
  def apply(s : (String, String, String)) : SLink = SLink(s._1, s._2, s._3)
  def apply(s : String, l : (String, String)) : SLink = SLink(s, l._1, l._2)
  def apply(l : (String, String), s : String) : SLink = SLink(l._1, l._2, s)
}

/**
  * Grants the ability to score two links.  Implicitly assumes the ability to score nodes
  */

trait LinkScorer extends NodeScorer with EdgeScorer {
  def link(l1 : SLink, l2 : SLink) : Double
}

/**
  *
  * Writing a generic shape extraction strategy isn't useful at this particular moment.
  * Before dealing with arbitrary shapes, need to produce a positive result on single predicates
  *
  * SPredicate is a simple way of storing a skeleton with a maxDepth of 1.
  *
  */
case class SPredicate(root : String, children : List[(String, String)]) extends Skeleton {
  def slinks : List[SLink] = children.map(l => SLink(root, l))

  def lisp : String = {
    val c = children.map(e => ':' + e._1 + " " + e._2).mkString(" ")
    "(%s %s)".format(root, c)
  }

  //convenient
  def key(f : (String) => Boolean = _ => true) : String = children.map(_._1).sorted.filter(f).mkString(",")
  override def predicates : List[SPredicate] = List(this)
  def g : DGraph[String, String] = {
    val (a,b) = children.zipWithIndex.map(eni => Node[String](eni._1._2, eni._2+1) -> DEdge[String](eni._1._1, eni._2+1, 0)).unzip

    DGraph.from(a.map(n => n.id -> n).toMap, new TreeMap[(Int, Int), DEdge[String]]().++(b.map(e => (e.from, e.to) -> e).toMap[(Int, Int), DEdge[String]]))
  }
}

/**
  * Generally speaking, a skeleton should be accessible as a DGraph and be breakable.  As it becomes necessary, I'll have
  * to add in a method that produces any skeleton type from a DGraph.  At the moment, we're only dealing with Predicates
  * so I'll keep track of it manually.
  */
trait Skeleton {
  def g : DGraph[String, String]

  /**
    * Allow skeletons to define their own translation functions -- they should come out as empty lists if the
    * transformation is invalid
    *
    */
  def predicates : List[SPredicate] = {
    val roots = g.outMap.filter(_._2.size > 0).unzip._1
    roots.map(r =>
      SPredicate(g.nodes(r).value, g.edges.filter(_._1._1 == r).toList.map(e => e._2.value -> g.nodes(e._1._2).value))
    )
  }.toList
}

/**
  * This is the guy that does all the scoring
  */
trait StructureScorer extends LinkScorer with EdgeAccepter {
  def verbose : Boolean
  // should be skeletons
  def corpus : List[Skeleton]
  def apply(g1 : Skeleton) : Double
  //def selectEach(g1 : Skeleton) : List[Skeleton]
}


