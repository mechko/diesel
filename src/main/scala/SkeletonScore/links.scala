package diesel.SkeletonScore.Scorers

import diesel.SkeletonScore.{LinkScorer, SLink}

object link {
  trait simplestrategy extends LinkScorer {
    override def link(l1 : SLink, l2 : SLink) : Double = {
      node(l1._1, l2._1) * edge(l1._2, l2._2) * node(l1._3, l2._3)
    }
  }

  trait distance extends simplestrategy with nodes.distance with edges.exact
  trait original extends simplestrategy with nodes.strategy1 with edges.exact
}