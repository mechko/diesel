package diesel.SkeletonScore.Scorers

import diesel.SkeletonScore.{SLink, SPredicate, StructureScorer}
import diesel.SkeletonScore.Skeleton


abstract class PredicateLevelStructScore extends StructureScorer {

  //bfs returns the best edgematching
  def bestMatch[T](l : Set[T], c : Set[T], sf : (T,T) => Double, memo : Map[(T, T), Double] = Map[(T,T), Double]()) : Map[(T, T), Double] = {
    if (l.size != c.size) Map()
    else if (l.size == 0) Map() //Reached the bottom of the recursion
    else l.map(_l => {
      c.map(_c => memo.updated((_l, _c), sf(_l,_c)) ++ bestMatch(l - _l, c - _c, sf)).maxBy(_.values.product)
    }).maxBy(_.values.product)
  }

  def scorePredicate(p : SPredicate, g : SPredicate) : Double = {

    if(verbose) {
      //println(p.lisp + " vs " + g.lisp)
    }

    // since each predicate is pretty small and this code will be overhauled when there are new shapes, just gonna
    // brute force this

    val bm = bestMatch[SLink](p.slinks.toSet.filter(a => accept(a._2)), g.slinks.toSet.filter(a => accept(a._2)), link)

    if(verbose) {
      //bm.map(a => a.copy(_1 = (a._1._1.lisp, a._1._2.lisp))).foreach(println)
      //println("-")
      //println(bm.values.product)
      //println("-")
    }

    bm.values.product
  }

  protected lazy val struct2pred : List[(String, Skeleton)] =
    corpus.flatMap(s => {
      s.predicates.map(sp => sp.key(accept) -> s)
    })

  protected lazy val hasPredStruct : Map[String, List[Skeleton]] = {
    struct2pred.groupBy(_._1).mapValues(_.map(sk => sk._2))
  }.withDefaultValue(List[Skeleton]())

  protected lazy val StructPreds : Map[Skeleton, Set[String]] = {
    struct2pred.map(_.swap).groupBy(_._1).mapValues(_.map(sk => sk._2).toSet)
  }.withDefaultValue(Set[String]())

  protected lazy val isPred : Map[String, Set[SPredicate]] = {
    corpus.flatMap(s => {
      s.predicates.map(sp => sp.key(accept) -> sp)
    }).groupBy(_._1).mapValues(_.map(sk=> sk._2).toSet)
  }.withDefaultValue(Set[SPredicate]())
}


/**
  * Each predicate in a skeleton is scored independently.
  */
abstract class IndependentBestPredicate(corpus : List[Skeleton]) extends PredicateLevelStructScore {
  def apply(g1 : Skeleton) : Double = {
    val bestp = g1.predicates.map(p => {
      isPred(p.key(accept)).map(o => o.lisp -> scorePredicate(p, o)).toMap.updated("nomatch", 0.0).maxBy(_._2)
    })

    if (verbose) {
      println("matching: ")
      bestp.foreach(println)
    }
    math.pow(bestp.unzip._2.::(1.0).product, 1.0/g1.predicates.size)
  }
}


/**
  * Each predicate in a skeleton is scored independently.
  */
abstract class IndependentWorstAndBestPredicate(corpus : List[Skeleton]) extends PredicateLevelStructScore {
  def apply(g1 : Skeleton) : Double = {
    val bestp = g1.predicates.map(p => {
      isPred(p.key(accept)).map(o => o.lisp -> scorePredicate(p, o)).toMap.updated("nomatch", 0.1).maxBy(_._2)
    })

    if (verbose) {
      println("matching: ")
      bestp.foreach(println)
    }

    val scores = bestp.unzip._2

    scores.::(0.0).max
  }
}

/**
  * Try each skeleton individually and choose the best score
  * @param corpus
  */
abstract class DependentBestSkeleton(corpus : List[Skeleton]) extends PredicateLevelStructScore {

  //this function is super duper inefficient rn (brute force)
  def apply(g1 : Skeleton) : Double = {
    val k = g1.predicates.map(s => s.key(accept) -> s).groupBy(_._1).mapValues(_.map(p => p._2).toSet)
    val res = corpus.map(g => {
      val targets = g.predicates.map(s => s.key(accept) -> s).groupBy(_._1).mapValues(_.map(p => p._2).toSet)
      g -> k.map(e => {
        targets.get(e._1) match { //Edgecases
          case Some(t) => bestMatch[SPredicate](e._2, t, scorePredicate).values.toList.+:(1.0).product
          case _ => 0.1 //smoothing?
        }
      }).toList.+:(0.0).max
    }).maxBy(_._2)

    if(verbose) {
      res._1.predicates.map(_.lisp).foreach(println)
      println()
      g1.predicates.map(_.lisp).foreach(println)
      println(math.pow(res._2, 1.0/g1.predicates.size))
    }
    math.pow(res._2, 1.0/g1.predicates.size)
  }
}


