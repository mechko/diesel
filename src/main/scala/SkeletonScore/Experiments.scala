package diesel.SkeletonScore

import dgraph.DGraph
import diesel.SkeletonScore.Scorers.{IndependentWorstAndBestPredicate, DependentBestSkeleton, PredicateLevelStructScore, IndependentBestPredicate}
import diesel.SkeletonScore.Scorers.link.{original, distance}


object Experiments {

  case class DSkeleton(g : DGraph[String, String]) extends Skeleton

  case class one(corpus : List[Skeleton], verbose : Boolean = false) extends IndependentWorstAndBestPredicate(corpus) with distance with AcceptAllEdges
  case class two(corpus : List[Skeleton], verbose : Boolean = false) extends IndependentWorstAndBestPredicate(corpus) with original with AcceptAllEdges
  case class three(corpus:List[Skeleton], verbose : Boolean = false) extends IndependentWorstAndBestPredicate(corpus) with distance with AcceptCommonEdges
  case class four(corpus: List[Skeleton], verbose : Boolean = false) extends IndependentWorstAndBestPredicate(corpus) with original with RejectCommonEdges
  case class five(corpus :List[Skeleton], verbose : Boolean = false) extends IndependentWorstAndBestPredicate(corpus) with distance with ReducedEdges
  case class six(corpus : List[Skeleton], verbose : Boolean = false) extends IndependentWorstAndBestPredicate(corpus) with original with ReducedEdges

  def scoreAll(gold : Map[String, Skeleton], parsed : Map[String, Skeleton], scorer : (List[Skeleton], Boolean) => PredicateLevelStructScore, verbose : Boolean = false) = {
      val res = gold.map(s => {
        val pd = parsed(s._1)
        val sc = scorer(gold.values.filterNot(_ == s._2).toList, verbose)

        sc(s._2) -> sc(pd) match {
          case (a, b) if a == b => None
          case (a, b) if a > b => Some(1)
          case (a, b) if a < b => Some(0)
          case _ => None
        }
      })

    val rec = res.size-res.count(_ == None)

    println(rec + "/" + res.size)
    println(res.count(_ == Some(1)) + "/" + rec + " = " + 1.0*res.count(_ == Some(1))/rec)

  }

}


object NewExperiment extends App {
  import diesel.Core._
  import FileOps._
  import diesel.parser._
  import diesel.parser.implicits._
  import diesel.SkeletonScore.Experiments._

  val gold = readLines("gold.data").getParseMap.map(a => a._1.trim() -> a._2).mapValues(g => g.getDGraph).filter(_._2.size == 1).mapValues(v => DSkeleton(v.head))
  val parsed = readLines("parsed.data").getParseMap.map(a => a._1.trim() -> a._2).mapValues(g => g.getDGraph).filter(_._2.size == 1).mapValues(v => DSkeleton(v.head))


  scoreAll(gold.filter(x => parsed.keySet.contains(x._1)), parsed, one.apply)

  scoreAll(gold.filter(x => parsed.keySet.contains(x._1)), parsed, two.apply)

  scoreAll(gold.filter(x => parsed.keySet.contains(x._1)), parsed, three.apply)

  scoreAll(gold.filter(x => parsed.keySet.contains(x._1)), parsed, four.apply)

  scoreAll(gold.filter(x => parsed.keySet.contains(x._1)), parsed, five.apply)

  scoreAll(gold.filter(x => parsed.keySet.contains(x._1)), parsed, six.apply)





}