package diesel.SkeletonScore.Scorers

import diesel.SkeletonScore.{SLink, NodeScorer}

/**
  * Created by mechko on 2/24/16.
  */
object nodes {
  /**
    *
    * takes two node scorers and returns a node scorer that combines them
    * this remains a trait so it can still be mixed in.  It also represents the
    * decision not to care about dynamic generation of node scorers.
    *
    */
  trait product extends NodeScorer {
    def n1 : NodeScorer
    def n2 : NodeScorer
    override def node(o1: String, o2: String): Double = n1.node(o1, o2) * n2.node(o1,o2)
  }

  trait distance extends NodeScorer{
    override def node(o1: String, o2: String): Double = {
      diesel.distance.pathDistP(o1, o2).getOrElse(0.0)
    }
  }

  trait maxSim extends NodeScorer {
    override def node(o1: String, o2: String): Double = diesel.distance.featureScore(o1,o2).getOrElse(0.0)
  }

  /**
    * Isn't it pretty?
    */

  trait strategy1 extends product {
    object n1 extends nodes.distance
    object n2 extends nodes.maxSim
  }
}
