package diesel.SkeletonScore.Scorers

import diesel.SkeletonScore.EdgeScorer

object edges {
  trait exact extends EdgeScorer {
    override def edge(e1: String, e2: String): Double = (e1 == e2) match {
      case true => 1.0
      case false => 0.0
    }
  }
}
