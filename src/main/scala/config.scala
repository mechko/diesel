package diesel

import com.typesafe.config._


// TODO:50 Change the name of the package to config and have sub objects so you can call, for example, diesel.config.wordnet, diesel.config.trips.root
object TripsConfig {
  private val config =  ConfigFactory.load()

  lazy val tripsXMLPath   = config.getString("diesel.tripsXML")
  lazy val tripsRoot      = config.getString("diesel.tripsRootPath")
  lazy val lispOntology   = tripsRoot + "/src/OntologyManager/Data/LFdata/"
  lazy val wordnetPath    = config.getString("diesel.wordnetPath")
  lazy val tripsParserLocal    = config.getString("diesel.tripsParserLocalURL")
  lazy val tripsParserRemote    = config.getString("diesel.tripsParserRemoteURL")

}
