import org.scalatest._
import diesel.TripsConfig._
import diesel.Core._
import diesel.Parenthood._


class Tests extends FlatSpec with Matchers {

  "config" should "find the ontology xml" in {

    println(tripsXMLPath)
    tripsXMLPath should be (scala.sys.env.getOrElse("tripsXMLPath", "fail"))

  }
  "core" should "load the ontology" in {

    ont.ontItems should not be empty
    //(ont !@ "bread").foreach(x => println(x.name))
  }

  "diesel" should "be able to test parenthood" in {

    println((ont-->("bread")).map(_ ^? "baked-goods"))
    println((ont-->("person")).map(_ ^? "baked-goods"))

  }
}
